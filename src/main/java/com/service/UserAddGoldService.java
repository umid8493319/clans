package com.service;

import com.dao.Clan;
import com.database.ClanDatabase;

public class UserAddGoldService {
    ClanDatabase database = new ClanDatabase();

    private final ClanService clans = new ClanService(); //for test

    public void addGoldToClan(long userId, long clanId, int gold) {
        Clan clan = clans.getClan(clanId);
        addValue(clan, userId, gold);
    }

    synchronized
    private void addValue(Clan clan, long userId, long value) {
        clan.addGold(value);
        database.saveChangeHistory( userId, clan, clan.getGold());
    }


}
