package com.database;

import com.dao.Clan;
import com.dao.ClanChangeHistory;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;


public class ClanDatabase {

    public void saveChangeHistory(long userId, Clan clan, long value) {
        try (Connection c = getConnection();
             Statement stmt = c.createStatement();
        ) {
            System.out.println("Opened database successfully");
            String sql = "INSERT INTO clan_info (user_id,clan_id,value) "
                    + "VALUES (" + userId + "," + clan.getId() + "," + value + ");";
            stmt.executeUpdate(sql);

        } catch (Exception e) {
            System.err.println(e.getClass().getName() + " : " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Records created successfully");
    }

    private Connection getConnection() throws SQLException {
        Connection c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/clans", "postgres", "1");
        c.setAutoCommit(false);
        return c;
    }

    public List<ClanChangeHistory> getHistory() {
        List<ClanChangeHistory> list = new ArrayList<>();
        try (Connection c = getConnection();
             Statement stmt = c.createStatement();
        ) {

            System.out.println("Opened database successfully");
            ResultSet rs = stmt.executeQuery("SELECT * FROM clan_info;");
            while (rs.next()) {
                int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                int clanId = rs.getInt("clan_id");
                int value = rs.getInt("value");
                Date date = rs.getDate("created_date");
                LocalDate localDate = date.toLocalDate();

                ClanChangeHistory clanChangeHistory = new ClanChangeHistory(id, userId, clanId, value, localDate);
                list.add(clanChangeHistory);

                System.out.println("ID = " + id);
                System.out.println();
            }
            rs.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Operation done successfully");
        return list;
    }

    public List<ClanChangeHistory> getClanHistory(Clan clan) {
        List<ClanChangeHistory> list = new ArrayList<>();
        try (Connection c = getConnection();
             Statement stmt = c.createStatement();
        ) {

            System.out.println("Opened database successfully");
            ResultSet rs = stmt.executeQuery("SELECT * FROM clan_info where id = " + clan.getId() + ";");
            while (rs.next()) {
                int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                int clanId = rs.getInt("clan_id");
                int value = rs.getInt("value");
                Date date = rs.getDate("created_date");
                LocalDate localDate = date.toLocalDate();

                ClanChangeHistory clanChangeHistory = new ClanChangeHistory(id, userId, clanId, value, localDate);
                list.add(clanChangeHistory);

                System.out.println("ID = " + id);
                System.out.println();
            }
            rs.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Operation done successfully");
        return list;
    }


    public Clan getClan(long clanId) {
        Clan clan = null;

        try (Connection c = getConnection();
             Statement stmt = c.createStatement();
        ) {
            System.out.println("Opened database successfully");
            String sql = "select * from clan where clan.id =  " + clanId + ");";

            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                long helper = rs.getLong("gold");
                AtomicLong gold = new AtomicLong(helper);
                clan = new Clan(id, name, gold);
            }
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + " : " + e.getMessage());
            System.exit(0);
        }
        return clan;
    }

    public void createClan(Clan clan) {
        try (Connection c = getConnection();
             Statement stmt = c.createStatement();
        ) {
            System.out.println("Opened database successfully");
            String sql = "INSERT INTO clan (name,gold) "
                    + "VALUES ('" + clan.getName() + "'," + clan.getGold() + ");";
            stmt.executeUpdate(sql);

        } catch (Exception e) {
            System.err.println(e.getClass().getName() + " : " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Records created successfully");
    }

    public List<Clan> getAll() {
        return null;//TODO
    }

}