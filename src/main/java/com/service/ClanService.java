package com.service;

import com.dao.Clan;
import com.database.ClanDatabase;

public class ClanService {

    ClanDatabase clanDatabase = new ClanDatabase();
    public Clan getClan(long clanId) {
        return clanDatabase.getClan(clanId);
    }
}
