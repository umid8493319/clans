package com.dao;

import java.util.concurrent.atomic.AtomicLong;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Clan {
        private long id;
        private String name;
        private AtomicLong gold;

    public long getGold() {
        return gold.get();
    }

    public synchronized void addGold(long gold){
        long l = this.gold.addAndGet(gold);
    }


}
