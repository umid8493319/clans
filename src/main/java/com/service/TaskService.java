package com.service;

public class TaskService {

    private final ClanService clans =  new ClanService();
    UserAddGoldService service = new UserAddGoldService();

    public void completeTask(long clanId, long taskId, Level level) {

        boolean success = getRandomBoolean();

        if (success) {
            System.out.println("TASK complete");
            int bonus = 0;
            switch (level) {
                case HIGH:
                    bonus = 1000;
                    break;
                case MEDIUM:
                    bonus = 500;
                    break;
                case LOW:
                    bonus = 100;
                    break;
            }
            service.addGoldToClan(taskId, clanId, bonus);
        } else {
            System.out.println("TASK is not complete");
        }
    }

    private boolean getRandomBoolean() {
        return Math.random() < 0.5;
    }

}


