package com.database;

import com.dao.Clan;
import com.dao.ClanChangeHistory;
import com.dao.User;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;


public class UserDatabase {

    private Connection getConnection() throws SQLException {
        Connection c = null;
        c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/clans", "postgres", "1");
        c.setAutoCommit(false);
        return c;
    }


    public User getUser(int userId) {
        User user = null;

        try (Connection c = getConnection();
             Statement stmt = c.createStatement();
        ) {
            System.out.println("Opened database successfully");
            String sql = "select * from user where user.id =  " + userId + ");";

            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");

                user = new User(id, name);
            }
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + " : " + e.getMessage());
            System.exit(0);
        }
        return user;
    }

    public void createUser(User user){
        try (Connection c = getConnection();
             Statement stmt = c.createStatement();
        ) {
            System.out.println("Opened database successfully");
            String sql = "INSERT INTO user (name) "
                    + "VALUES ('" + user.getName() + "'" + ");";
            stmt.executeUpdate(sql);

        } catch (Exception e) {
            System.err.println(e.getClass().getName() + " : " + e.getMessage());
            System.exit(0);
        }
        System.out.println("User created successfully");
    }

    public List<User> getAll(){
        return null;//TODO
    }


}