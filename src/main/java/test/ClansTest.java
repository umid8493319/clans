package test;

import com.dao.Clan;
import com.dao.ClanChangeHistory;
import com.database.ClanDatabase;
import com.service.Level;
import com.service.TaskService;
import com.service.UserAddGoldService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ClansTest {
    UserAddGoldService service;
    TaskService taskService;
    ClanDatabase clanDatabase;
    Random random;

    @BeforeEach
    void setUp() {
        service = new UserAddGoldService();
        taskService = new TaskService();
        clanDatabase = Mockito.mock(ClanDatabase.class);
        random = new Random();
    }


    @SneakyThrows
    @Test
    void clanTest() {
        ExecutorService executor = Executors.newFixedThreadPool(100);

        int clanId = 2;

        for (int i = 0; i < 100; i++) {

            int userId = random.nextInt(10) + 5;
            int value = 50;
            Runnable runnable = () -> service.addGoldToClan(userId, clanId, value);
            executor.execute(runnable);
        }

        executor.shutdown();
        executor.awaitTermination(10, TimeUnit.MINUTES);

        taskService.completeTask(2, 53, Level.HIGH);
        taskService.completeTask(2, 52, Level.MEDIUM);
        taskService.completeTask(2, 51, Level.LOW);

        Thread.sleep(6000);

        System.out.println("Test");

        ClanDatabase clanDatabase = new ClanDatabase();
        List<ClanChangeHistory> info = clanDatabase.getHistory();

        info.forEach(i -> {
            System.out.println(i.getValue());
        });

        Clan clan = clanDatabase.getClan(2);

        //value 50 * 100 threads

        assertTrue(clan.getGold()==5000);
    }
}
