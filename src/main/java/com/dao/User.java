package com.dao;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class User {
    private long id;
    private String name;
}
