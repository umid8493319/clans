package com.dao;

import lombok.*;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ClanChangeHistory {
    private long id;
    private long userId;
    private long clanId;
    private long value;
    private LocalDate date;
}
