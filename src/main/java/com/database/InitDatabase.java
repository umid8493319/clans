package com.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class InitDatabase {

   public void init(){

        try(Connection conn = getConnection();
            Statement stmt = conn.createStatement();
        ) {

          String  sql = "CREATE TABLE IF NOT EXISTS public.clan\n" +
                    "(\n" +
                    "    id integer NOT NULL,\n" +
                    "    name character varying COLLATE pg_catalog.\"default\",\n" +
                    "    CONSTRAINT clan_pkey PRIMARY KEY (id)\n" +
                    ")";
            stmt.executeUpdate(sql);
            System.out.println("Table clan created successfully...");
            sql = "CREATE TABLE IF NOT EXISTS public.\"user\"\n" +
                    "(\n" +
                    "    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),\n" +
                    "    name character varying COLLATE pg_catalog.\"default\",\n" +
                    "    CONSTRAINT user_pkey PRIMARY KEY (id)\n" +
                    ")\n";
            stmt.executeUpdate(sql);
            System.out.println("Table user created successfully...");
            sql = "CREATE TABLE IF NOT EXISTS public.clan_info\n" +
                    "(\n" +
                    "    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),\n" +
                    "    user_id integer,\n" +
                    "    clan_id integer,\n" +
                    "    value integer,\n" +
                    "    created_date date NOT NULL DEFAULT CURRENT_TIMESTAMP,\n" +
                    "    CONSTRAINT clan_info_pkey PRIMARY KEY (id)\n" +
                    ")\n";
            stmt.executeUpdate(sql);
            System.out.println("Table clan_info created successfully...");

            System.out.println();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private Connection getConnection() throws SQLException {
        Connection c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/clans", "postgres", "1");
        c.setAutoCommit(false);
        return c;
    }

}
